From: Alex King <alex@king.net.nz>
Date: Sun, 15 Jun 2008 16:49:29 +0200
Subject: [PATCH] Add manpage

From Debian changelog, version 0.71-5: added manpage from Alex King
(alex@king.net.nz) THANKS! Closes: Bug#4309
---
 nacctd.8 |  201 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 1 files changed, 201 insertions(+), 0 deletions(-)
 create mode 100644 nacctd.8

diff --git a/nacctd.8 b/nacctd.8
new file mode 100644
index 0000000..2e6ba82
--- /dev/null
+++ b/nacctd.8
@@ -0,0 +1,201 @@
+.\" (C) Copyright 2001 Alex King (alex@king.net.nz)
+.\"
+.\" This is free documentation; you can redistribute it and/or
+.\" modify it under the terms of the GNU General Public License as
+.\" published by the Free Software Foundation; either version 2 of
+.\" the License, or (at your option) any later version.
+.\"
+.\" The GNU General Public License's references to "object code"
+.\" and "executables" are to be interpreted as the output of any
+.\" document formatting or typesetting system, including
+.\" intermediate and printed output.
+.\"
+.\" This manual is distributed in the hope that it will be useful,
+.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
+.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+.\" GNU General Public License for more details.
+.\"
+.\" You should have received a copy of the GNU General Public
+.\" License along with this manual; if not, write to the Free
+.\" Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111,
+.\" USA.
+.\"
+.\" Fri Apr  6 01:27:28 NZST 2001 Alex King <alex@king.net.nz> initial release
+.\" Sat Apr  7 10:20:53 NZST 2001 Alex King <alex@king.net.nz> cleaned up
+.\" with the help of Colin Watson
+.\"
+.TH nacctd 8 "16 Dec 2001"
+.SH NAME
+nacctd \- network accounting daemon
+.SH SYNOPSIS
+.B nacctd
+.IB [ \-dD "] [" \-c " filename]"
+.SH DESCRIPTION
+The network accounting daemon logs network traffic in a format
+suitable for generating billing information or usage statistics.
+.B nacctd
+listens on network interfaces and periodically writes information
+to a log file.
+
+.B nacctd
+is configured by editing its configuration file,
+.BR /etc/nacctd.conf .
+
+.SH OPTIONS
+.TP
+.B -d
+This will let nacctd run in debug mode
+.TP
+.B -D
+This will make nacctd not to detach as a daemon, suitable for running it
+from inittab.
+.TP
+.B -c
+.I configfile
+Specify the path of an alternative config file.
+
+.SH CONFIGURATION FILE OPTIONS
+.TP
+.B flush \fI<n>\fR
+Flush every \fIn\fR seconds. This gives the interval in seconds when the
+accumulated data is flushed to the output file. Typically set to 300
+(five minutes).
+.TP
+.B fdelay \fI<n>\fR
+This defines after how many seconds of inactivity a certain record of
+traffic information may be written out. This helps making the log files
+smaller since only one output record will be generated for related
+traffic.  Typically set to 60 seconds.
+.TP
+.B file \fI<f>\fR
+Specifies the main output file for the daemon to log network traffic to.
+.TP
+.B dumpfile \fI<f>\fR
+Specifies a file to dump data to that is not yet written to the main
+output file.  This is to prevent data loss should a crash occur.  On
+startup an existing file of this name will be moved to <f>.o
+.TP
+.B notdev \fI<interface>
+Don't log entries for this interface.
+.TP
+.B device \fI<interface>
+Specifies a network interface to put into promiscuous mode.
+.TP
+.B iflimit \fI<interface>
+Log only packets on this interface.  Mutually exclusive with
+.B hostlimit.
+.TP
+.B ignoremask \fI<netmask>
+Specifies a netmask (in dotted quad format) for which traffic
+is ignored.  This allows traffic on the local LAN to be excluded.
+.TP
+.B ignorenet \fI<network> <netmask>
+Ignore traffic on this network. Ignoring a net with ignorenet is not
+as efficient as ignoremask. Thus you should exclude your local network
+with ignoremask in preference to ignorenet.
+.TP
+.B masqif \fI<ipaddr>
+Specifies an ip number we are masquerading as.  This re-maps ip/port
+for incoming connections (e.g. FTP-data) to ip/port of the masqueraded
+destination.
+.TP
+.B debug \fI<n>
+Sets the debugging level to \fI<n>\fR.
+.TP
+.B headers \fI<interface-type> <data-start> <type-field>
+Defines where the real data starts for each type of interface.
+\fI<interface-type>\fR is one of eth, lo, plip, isdn etc.
+\fI<data-start>\fR is the offset in bytes to the start of the real data.
+\fI<type-field>\fR is the offset of the type field in bytes, or a 0 if
+there is no type field.  If SLIP or PPP devices are specified here,
+association of dynamic ip addresses with usernames won't work (see
+\fIdynamicip\fR below).
+.TP
+.B dynamicip \fI<dir>
+Specifies a directory to get username information from, where users
+are logged into ppp or slip accounts and assigned dynamic ip addresses.
+The directory should contain
+a file for each logged in user, where the filename is their
+IP address, and the file contains their username.
+Typically, these files will be created by ip-up scripts.
+.TP
+.B dynamicnet \fI<network> <netmask>
+Specifies the network the slip/ppp dynamic ips are assigned from.
+.TP
+.B exclude-name-lookup \fI<network> <netmask>
+Specifies a (sub)net to exclude from dynamic ip name lookup.
+.TP
+.B hostlimit \fI<ipaddr>
+Log only packets to/from this host.  This may be specified multiple
+times for multiple hosts.  This option is mutually exclusive with
+\fIiflimit\fR.
+.TP
+.B disable \fI<n>
+Don't include field \fI<n>\fR in the output format.
+.TP
+.B dontignore \fI<network> <netmask>
+Don't ignore hosts on the specified (sub)net that would otherwise have
+been excluded by an ignorenet statement.  This can be a useful to
+account for proxy traffic by specifying the proxy servers' subnet.
+.TP
+.B line \fI <interface> <device>
+Specifies fixed mapping of slip/ppp interface names to tty devices.
+This is used to assign traffic to a user if nacctd runs on the
+ppp/slip server and the relation between network interface and serial
+line is fixed.  This option is obsolete.
+.SH "OUTPUT FILE FORMAT"
+The output file consists of lines with up to 10 fields, or less if the
+configuration file disables one or more fields.
+
+\fItimestamp protocol src-addr src-port dst-addr dst-port count size user interface\fR
+.TP
+\fItimestamp
+Time in seconds past the epoch (standard UNIX time format)
+.TP
+\fIprotocol
+IP protocol
+.TP
+\fIcount
+count of packets
+.TP
+\fIsize
+size of data
+.TP
+\fIuser
+associated user in case of a slip/ppp link, this will always be
+"unknown" for other interfaces.
+.LP
+If the type is an ICMP message, field 4 is the ICMP message type and field
+6 is the ICMP message code.
+
+Please note that for forwarded packets there will be one line for EACH
+interface the packet passed. So if you are running this on your slip-server
+you will get all the traffic over the slip interfaces TWICE, once for the sl*
+devices and once for the eth* device. The same goes for ppp and generally for
+all forwarded traffic.  You can specify with 'notdev' entries which
+interfaces you don't want to see in the log.
+.SH FILES
+.TP
+/etc/nacctd.conf
+Configuration file
+.TP
+/var/log/net-acct
+Default location for the main output file
+.TP
+/var/log/net-acct-dump
+Default location for the dump of data not yet written to the main
+file.
+.SH "SEE ALSO"
+/usr/share/doc/net-acct/*,
+.BR tcpdump (8),
+.BR trafshow (1).
+.SH CAVEATS
+This manual page is incomplete, and possibly inaccurate.
+.SH AUTHORS
+Ulrich Callmeier
+
+Richard Clark <rclark@ethos.co.nz>
+
+This manual page was written by Alex King <alex@king.net.nz>,
+for the Debian GNU/Linux system, using material from the original
+documentation.
-- 
