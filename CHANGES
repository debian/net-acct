From 0.1 to 0.1a
 - removed a silly bug that prevented the lockfiles for dumping not to
   be erased when the dump was empty. This left a lot of empty lockfiles
   in /tmp. Arghhhh.

From 0.1a to 0.2
 - removed a race condition in the signal handling (SIGCHLD).
   This was introduced when I added the dump process, so it is
   possible for both children to exit at the same time.
   This resulted in a zombie dump process on slow or heavy loaded
   machines.
 - Added pid file creation (/var/run/nacctd.pid a la crond) and
   detection. 

From 0.2 to 0.3
 - made output more configurable. Thanks for inspiration from
   Scott Penrose <scottp@pas.com.au>.

From 0.3 to 0.4
 - added a new way of handling ip - user assignment. Thanks
   for the idea and help in debugging from Bart Kindt <bart@es.co.nz>
 - added a dontignore option
 - cleaned up debugging output
 - made debugging output more configurable
 - default for ignoremask is now really 255.255.255.255 as mentioned in the
   documentation, , i.e. don't ignore anything. You might want to add
   a ignoremask 255.255.255.0 to existing naccttabs, to match the previous
   default
 - fixed a few races in child/signal handling

From 0.4 to 0.5
 - please notice my new email-address uc@coli.uni-sb.de
 - there is now support for libpcap. I've tested this with libpcap-0.2
   and Solaris 2.4. It should work with other Unices with small changes.
 - enhancements by Bernd Eckenfels (ecki@lina.inka.de)
   - new notdev-option to disable logging for certain devices
   - no-detach option (-D) to allow for running from inittab
   - added count column for a packet count
   - you can now disable the user column in the output
   - memory usage reduced for "unknown"-case
 - support for token-ring and ISDN (thanks to Olaf (root@sel05.bertelsmann.de)
   for token ring and to Herbert Rosmanith and the various people on the
   isdn4linux-mailinglist for ISDN)
 - fixed a bug with the dynamic-net feature. Thanks to 
   Andreas Heilwagen and Sebastian Schaefer for finding this one.
   I already reported this bug on the mailing-list.
 - enhancements by Vlad Seriakov
   - compile-time option to make time column human readable
   - Get username for local tcp connections. This is a compile-time
     option

from 0.5 to 0.6 (first 'unofficial' release) 
- new configuration file option: hostlimit <ip address>
  Limits packet logging to ONLY the specified IP address(es). Overrides
  all other limits such as ignorenet etc. Multiple hostlimit tags can be
  given. Useful for monitoring colocated hosts, dedicated P-to-P links,
  etc. mutually exclusive with the iflimit tag.

- added hash table patches from Tom Brown <tbrown@baremetal.com>
  with configurable hash size in Makefile (default 256)

- iflimit tag (based on patches from Nigel Metheringham 
  <Nigel.Metheringham@theplanet.net>) allows you to limit which interfaces
  we monitor for traffic. similar to the hostlimit tag and mutually exclusive
  with it.

- masquerade support added, also contributed by Nigel Metheringham

- SIGHUP now causes nacctd to reread the config file

from 0.6 to 0.7:

- revised docs and Makefile slightly (added -O2 etc)
- increased default hash table size and improved algorithm
- added ICMP masquerading and masq bug fixes
- fixed bug with multiple notdev definitions in naccttab
- added PLIP header entry to naccttab.sample
- fixed disable entry in naccttab from field 8 to 7 for 0.5 compatibility
- fixed problem with promisc devices not being reinitialized on SIGHUP
